package com.lshift.arrivals

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

class DisplayServiceTest {

    private val baseTime = LocalDateTime.of(LocalDate.of(2018, 7, 13), LocalTime.of(10,0,0))

    private val examples = listOf(
        ExpectedArrival("station 1", baseTime),
        ExpectedArrival("station 2", baseTime.plusMinutes(5)),
        ExpectedArrival("station 3", baseTime.plusMinutes(10)),
        ExpectedArrival("station 4", baseTime.plusMinutes(15)),
        ExpectedArrival("station 5", baseTime.plusMinutes(20)),
        ExpectedArrival("station 6", baseTime.plusMinutes(25)),
        ExpectedArrival("station 7", baseTime.plusMinutes(30)),
        ExpectedArrival("station 8", baseTime.plusMinutes(35))
    )

    private val fakeSource = object : TubeSource {
        override fun getArrivals(lineId: String, stopPointId: String): List<ExpectedArrival> {
            if (lineId == "lineId" && stopPointId == "stopId") {
                return examples
            } else {
                throw RuntimeException("Bad line/stop id")
            }
        }
    }

    @Test
    fun `can get next arrival given current local time`() {

        val unit = DisplayService(fakeSource, "lineId", "stopId")
        assertThat(unit.announcementAt(baseTime), equalTo("Train arriving at platform is for station 1"))
        assertThat(unit.announcementAt(baseTime.plusMinutes(5)), equalTo("Train arriving at platform is for station 2"))
        assertThat(unit.announcementAt(baseTime.plusMinutes(10)), equalTo("Train arriving at platform is for station 3"))
    }

    @Test
    fun `will not provide announcement until time has expected time has passed`() {

        val unit = DisplayService(fakeSource, "lineId", "stopId")
        assertThat(unit.announcementAt(baseTime.plusMinutes(5)), equalTo("Train arriving at platform is for station 2"))
        assertThat(unit.announcementAt(baseTime.plusMinutes(6)), nullValue())
        assertThat(unit.announcementAt(baseTime.plusMinutes(7)), nullValue())
        assertThat(unit.announcementAt(baseTime.plusMinutes(8)), nullValue())
        assertThat(unit.announcementAt(baseTime.plusMinutes(9)), nullValue())
        assertThat(unit.announcementAt(baseTime.plusMinutes(10)), equalTo("Train arriving at platform is for station 3"))
        assertThat(unit.announcementAt(baseTime.plusMinutes(11)), nullValue())
    }

    @Test
    fun `will not provide same announcement twice`() {

        val unit = DisplayService(fakeSource, "lineId", "stopId")
        assertThat(unit.announcementAt(baseTime.plusMinutes(5)), equalTo("Train arriving at platform is for station 2"))
        assertThat(unit.announcementAt(baseTime.plusMinutes(5)), nullValue())
    }

    @Test
    fun `can get list of upcoming arrivals`() {

        val unit = DisplayService(fakeSource, "lineId", "stopId")
        assertThat(unit.upcomingArrivals(baseTime), equalTo(listOf(
            "Train for station 2 arrives in 5 minutes",
            "Train for station 3 arrives in 10 minutes",
            "Train for station 4 arrives in 15 minutes"
        )))

    }

    @Test
    fun `when arrival due with in a minute, change message to reflect this`() {

        val unit = DisplayService(fakeSource, "lineId", "stopId")
        assertThat(unit.upcomingArrivals(baseTime.plusMinutes(4).plusSeconds(30)), equalTo(listOf(
            "Train for station 2 due now",
            "Train for station 3 arrives in 5 minutes",
            "Train for station 4 arrives in 10 minutes"
        )))

    }


}