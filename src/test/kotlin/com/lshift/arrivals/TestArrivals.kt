package com.lshift.arrivals


import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.Matchers
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status
import org.http4k.core.Uri
import org.junit.Assert.assertThat
import org.junit.Test
import java.io.BufferedReader
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

class TestArrivals {

    @Test
    fun `can get arrivals for line and stop id`() {
        //https://api.tfl.gov.uk/Line/bakerloo/Arrivals/940GZZLUWLO?direction=inbound

        val unit = TflDataSource(::fakeUpstream)
        val arrivals: List<ExpectedArrival> = unit.getArrivals("bakerloo", "940GZZLUWLO")
        assertThat(arrivals.size, Matchers.greaterThan(3))

        assertThat(arrivals[0].destinationName, equalTo("Elephant & Castle Underground Station"))
        assertThat(arrivals[0].expectedArrival, equalTo(LocalDateTime.of(LocalDate.of(2018,7,13), LocalTime.of(10,7,32))))
    }


    //assertThat(ExpectedArrival("this is a dummy"), equalTo(ExpectedArrival("this is a dummy")))


    private fun fakeUpstream(r: Request): Response =
        when (r.uri) {
            Uri.of("/Line/bakerloo/Arrivals/940GZZLUWLO?direction=inbound") ->
                Response(Status.OK).header("content-type", "application/json").body(loadData("/test-arrivals.json"))
            else -> Response(Status.NOT_FOUND)
        }

}

fun loadData(filename: String) = Any()::class.java.getResourceAsStream(filename).bufferedReader().use(BufferedReader::readText)