package com.lshift.arrivals

import java.time.Duration
import java.time.LocalDateTime

class DisplayService(val source: TubeSource, val lineId: String, val stopPointId: String) {

    private val alreadyAnnounced = mutableListOf<ExpectedArrival>()

    fun announcementAt(timeNow: LocalDateTime): String? {

        val arrivals = source.getArrivals(lineId, stopPointId)
        val next = arrivals.filter { timeNow >= it.expectedArrival }.lastOrNull()

        return if (next != null && !alreadyAnnounced.contains(next)) {
            alreadyAnnounced.add(next)
            "Train arriving at platform is for ${next.destinationName}"
        } else {
            null
        }
    }

    fun upcomingArrivals(timeNow: LocalDateTime): List<String> {
        val arrivals = source.getArrivals(lineId, stopPointId)

        return arrivals.filter { timeNow < it.expectedArrival }.take(3).map {
            val minutes = timeDiff(timeNow, it.expectedArrival)
            if (minutes < 1) {
                "Train for ${it.destinationName} due now"
            } else {
                "Train for ${it.destinationName} arrives in $minutes minutes"
            }
        }
    }

    private fun timeDiff(a: LocalDateTime, b: LocalDateTime) = Duration.between(a, b).toMinutes().toInt()

}