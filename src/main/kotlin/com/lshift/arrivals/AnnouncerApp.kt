package com.lshift.arrivals

import org.http4k.client.ApacheClient
import org.http4k.core.Uri
import org.http4k.core.then
import org.http4k.filter.ClientFilters
import java.time.LocalDateTime

fun main(args: Array<String>) {

    if (args.size != 2) {
        println("usage: <StopPoint Id> <line Id>")
        System.exit(1)
    }

    val stopPointId=args[0]
    val lineId=args[1]
    println("Announcer App")
    println("StopPoint ID: $stopPointId")
    println("line ID: $lineId")

    val client = CacheForAWhile().then(ClientFilters.SetHostFrom(Uri.of("https://api.tfl.gov.uk")).then(ApacheClient()))
    val tubeSource: TubeSource = TflDataSource(client)

    val externalSystem = MockedExternalSystem()
    val displayService = DisplayService(tubeSource, lineId, stopPointId)

    while(true) {
        val time = LocalDateTime.now()
        val x = tubeSource.getArrivals(lineId, stopPointId)
//        println(x.first())
//        println(time)
        val announcement = displayService.announcementAt(time)
        if (announcement != null) {
            externalSystem.send_arrival(announcement)
        }

        externalSystem.send_following_arrivals(displayService.upcomingArrivals(time))
        Thread.sleep(5000)
    }
}