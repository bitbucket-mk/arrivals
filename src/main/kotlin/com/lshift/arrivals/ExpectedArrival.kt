package com.lshift.arrivals

import java.time.LocalDateTime

data class ExpectedArrival(val destinationName: String, val expectedArrival: LocalDateTime)