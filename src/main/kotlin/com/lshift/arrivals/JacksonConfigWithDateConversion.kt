package com.lshift.arrivals

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import org.http4k.format.ConfigurableJackson
import org.http4k.format.defaultKotlinModuleWithHttp4kSerialisers
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.ZoneId

val dateTimeLocal: SimpleModule = defaultKotlinModuleWithHttp4kSerialisers.addDeserializer(LocalDateTime::class.java,
    object : JsonDeserializer<LocalDateTime>() {
        override fun deserialize(p: JsonParser, ctxt: DeserializationContext): LocalDateTime {
            return LocalDateTime.ofInstant(SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX").parse(p.text.trim()).toInstant(), ZoneId.systemDefault())
        }
    }
)

object JacksonConfigWithDateConversion : ConfigurableJackson(ObjectMapper()
    .registerModule(dateTimeLocal)
    .disableDefaultTyping()
    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    .configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false)
    .configure(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS, true)
    .configure(DeserializationFeature.USE_BIG_INTEGER_FOR_INTS, true)
)