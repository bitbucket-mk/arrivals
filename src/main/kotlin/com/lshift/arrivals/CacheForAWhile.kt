package com.lshift.arrivals

import org.http4k.core.Filter
import org.http4k.core.HttpHandler
import org.http4k.core.Request
import org.http4k.core.Response

class CacheForAWhile: Filter {

    private var cached: Response? = null
    private var expireAt: Long = System.currentTimeMillis()

    override fun invoke(next: HttpHandler): HttpHandler = {
        request: Request ->
            synchronized(this) {
                if (cached == null || System.currentTimeMillis() > expireAt) {
                    println("Refreshing cache")
                    cached = next(request)
                    expireAt = System.currentTimeMillis() + 60000 * 5
                }
            }
            cached!!
    }
    }