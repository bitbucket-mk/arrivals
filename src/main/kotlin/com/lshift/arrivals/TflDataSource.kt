package com.lshift.arrivals

import org.http4k.core.Body
import org.http4k.core.HttpHandler
import org.http4k.core.Response
import com.lshift.arrivals.JacksonConfigWithDateConversion.auto
import org.http4k.core.Method.GET
import org.http4k.core.Request

interface TubeSource {
    fun getArrivals(lineId: String, stopPointId: String): List<ExpectedArrival>
}

class TflDataSource(val client: HttpHandler): TubeSource {

    override fun getArrivals(lineId: String, stopPointId: String): List<ExpectedArrival> {
        return toArrivals(client(Request(GET, "/Line/$lineId/Arrivals/$stopPointId?direction=inbound")))
    }

    companion object {
        private val arrivalsLens = Body.auto<Array<ExpectedArrival>>().toLens()
        fun toArrivals(body: Response): List<ExpectedArrival> = arrivalsLens.extract(body).toList().sortedBy { it.expectedArrival }
    }

}