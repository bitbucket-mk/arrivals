package com.lshift.arrivals

interface ExternalSystem {
    fun send_arrival(text: String)
    fun send_following_arrivals(following: List<String>)
}