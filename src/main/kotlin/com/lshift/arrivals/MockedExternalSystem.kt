package com.lshift.arrivals

class MockedExternalSystem: ExternalSystem {
    override fun send_following_arrivals(following: List<String>) {
        println("Following trains >>> ")
        following.forEach { println(it) }
        println("<<<\n\n")
    }

    override fun send_arrival(text: String) {
        println("Arrival announcement >>>   $text")
    }

}